# Arma 3 Liberation - Silly Edition
A simple edit of liberation to turn off some restrictions with the default Liberation. Made for a friend.

## Changes
- [x] Disabling civilian kill penalties/notifications
- [x] Disabling player kill penalties/notifications
- [x] Enable scoring